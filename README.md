# Algoritmo de búsqueda con diferentes estrategias - Python 3.10.6
Proyecto desarrollado como parte de la asignatura **[42321	- Sistemas Inteligentes](https://guiae.uclm.es/vistaGuia/407/42321)** en el tercer curso de ingeniería informática. En este trabajo, hemos desarrollado un algoritmo en conjunto entre tres estudiantes, el cual toma un grafo en formato XML como entrada, y se realiza una búsqueda utilizando las estrategias vistas en la parte de teoría de la asignatura (anchura, profundidad, coste uniforme, voraz y A*). Partiendo de una localización inicial, deberemos visitar una serie de puntos de interés.

## Tecnologías Utilizadas
**Lenguaje de programación**: Python 3.10.6 y XML(datos del grafo y librería SAX para leerlos)  
**Herramientas de desarrollo**: Visual Studio Code  

Más información en los PDF's asociados.
